//
//  File.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import Foundation

protocol Reuseble {
    
}

extension Reuseble {
    
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
}
