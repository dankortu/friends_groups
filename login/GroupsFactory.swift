//
//  MakeGroupsArray.swift
//  login
//
//  Created by Daniil Kortunov on 31.03.2021.
//

import UIKit

enum GroupsFactory{
    case myGroups
    case allGroups
    
    static func makeGroupsArr(mode: GroupsFactory) -> [GroupModel] {
        var groups = [GroupModel]()
        
        groups.append(GroupModel(avatar: UIImage(named: "group_1")!, name: "Хурма", subscribersNumber: 68_000, id: 88005553535, isSubscribed: true))
        groups.append(GroupModel(avatar: UIImage(named: "group_2")!, name: "Lurkmore", subscribersNumber: 524_467, id: 123123123))
        groups.append(GroupModel(avatar: UIImage(named: "group_3")!, name: "ЗА БПАН ДУШУ ПРОДАМ", subscribersNumber: 14, id: 14881488))
        groups.append(GroupModel(avatar: UIImage(named: "group_4")!, name: "Мусора сосать!", subscribersNumber: 11_326, id: 345348953))
        groups.append(GroupModel(avatar: UIImage(named: "group_5")!, name: "Akira fun club", subscribersNumber: 22_678, id: 325348953, isSubscribed: true))
        groups.append(GroupModel(avatar: UIImage(named: "group_6")!, name: "Дискотека 80-х", subscribersNumber: 12_000, id: 494_134_892, isSubscribed: true))
        groups.append(GroupModel(avatar: UIImage(named: "group_7")!, name: "40 кг ;)", subscribersNumber: 520_567, id: 781_345_967))
        groups.append(GroupModel(avatar: UIImage(named: "group_8")!, name: "ХАБИБ!", subscribersNumber: 140_000, id: 148_953_312))
        groups.append(GroupModel(avatar: UIImage(named: "group_9")!, name: "Соловьев LIVE", subscribersNumber: 112_366, id: 159_875_462, isSubscribed: true))
        groups.append(GroupModel(avatar: UIImage(named: "group_10")!, name: "Свободу Навальному!", subscribersNumber: 2_200_678, id: 327_328_333))
        
        switch mode {
        case .myGroups:
            var myGroups = [GroupModel]()
            
            for i in (0 ..< groups.count) {
                if groups[i].isSubscribed {
                    
                    myGroups.append(groups[i])
                }
            }
            
            return myGroups
        case .allGroups:
        return groups
        
        }
    }
}
