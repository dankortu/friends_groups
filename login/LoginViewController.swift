//
//  ViewController.swift
//  login
//
//  Created by Daniil Kortunov on 29.03.2021.
//

import UIKit



final class LoginViewController: UIViewController {
    
    
    private let login = UITextField()
    private let passwordTextField = UITextField()
    private let loginButton = UIButton()
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(login)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        
        setupTextFieldPropperties(textField: login, mode: .login)
        setupTextFieldPropperties(textField: passwordTextField, mode: .password)
        setupButtonProperties(button: loginButton)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


}

extension LoginViewController {
    enum FieldType {
        case password
        case login
    }
    
    func setupTextFieldPropperties(textField: UITextField, mode: FieldType) {
        var constraintsArr = [NSLayoutConstraint]()
        
        let constraintWidthPortrait = textField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -60)
        constraintWidthPortrait.priority = UILayoutPriority(995)
        let constraintWidthLandscape = textField.widthAnchor.constraint(lessThanOrEqualToConstant: 500)
        constraintWidthLandscape.priority = UILayoutPriority(999)
        constraintsArr.append(constraintWidthPortrait)
        constraintsArr.append(constraintWidthLandscape)
        
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor(red: 0.75, green: 0.83, blue: 0.95, alpha: 1.00)
        
        textField.layer.cornerRadius = 7
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.darkGray.cgColor
    
        constraintsArr.append(textField.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        
        switch mode {
        case .login:
            textField.placeholder = "Введите логин"
            constraintsArr.append(textField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30))
        case .password:
            textField.placeholder = "Введите пароль"
            textField.isSecureTextEntry = true
            constraintsArr.append(textField.topAnchor.constraint(equalTo: login.bottomAnchor, constant: 20))
        }
        
        
        NSLayoutConstraint.activate(constraintsArr)
    }
    
    func setupButtonProperties(button: UIButton) {
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.setTitle("Войти", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.darkGray, for: .highlighted)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.darkGray.cgColor
        
        button.backgroundColor = UIColor(red: 0.76, green: 0.88, blue: 0.77, alpha: 1.00)
        
        button.addTarget(self, action: #selector(actionTouchUpLoginButton), for: .touchUpInside)
        
        
        
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.4),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20)
        ])
    }
    
    
    @objc
    func actionTouchUpLoginButton() {
        login.text = nil
        passwordTextField.text = nil
        
        let tabBarVC = HomeTabVC()
        let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
        mySceneDelegate?.setNewViewController(viewController: tabBarVC)
        
    }

}

