//
//  UserPhotoVC.swift
//  login
//
//  Created by Daniil Kortunov on 01.04.2021.
//

import UIKit

final class UserPhotoVC: UIViewController {
    
    //MARK: PROPERTIES
    
    private var userPhotoIV = UIImageView()
    private var setAvatarButton = UIButton()
    private var likeControl = LikeControl()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewIfNeeded()
        
        view.addSubview(userPhotoIV)
        view.insertSubview(likeControl, aboveSubview: userPhotoIV)
        view.addSubview(likeControl)
        view.backgroundColor = .black
        
        setupPropertiesLikeControl()
        setupPropertiesUserPhotoIV()
    }
        
    
    func setSelectedPhoto(_ image: UIImage) {
        userPhotoIV.image = image
    }

}

extension UserPhotoVC {
    
    private func setupPropertiesLikeControl() {
       
        likeControl.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            likeControl.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            likeControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            likeControl.heightAnchor.constraint(equalToConstant: 35),
            likeControl.widthAnchor.constraint(equalToConstant: 70)
        ])
    }
    
    private func setupPropertiesUserPhotoIV() {
        
        userPhotoIV.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            userPhotoIV.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            userPhotoIV.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            userPhotoIV.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        
        
        userPhotoIV.contentMode = .scaleAspectFit
    }
    

    
}
