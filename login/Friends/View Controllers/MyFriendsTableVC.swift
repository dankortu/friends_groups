//
//  FriendsViewController.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class MyFriendsTableVC: UITableViewController {
    
    // MARK: - Properties
    
    private var friends = [UserModel]()
    
    private var friendsDictionary = [String: [UserModel]]()
    private var friendsSectionTitles = [String]()
    
    
    private var filteredFriends = [UserModel]()
    private var filteredSectionTitle: String = ""
    private var searchController: UISearchController!
    
    private var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    private var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Мои друзья"
        searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        createFriendsArray()
        filteredFriends = friends
        
        for friend in friends {
            let friendKey = String(friend.name.prefix(1))
            if var friendValues = friendsDictionary[friendKey] {
                friendValues.append(friend)
                friendsDictionary[friendKey] = friendValues
            } else {
                friendsDictionary[friendKey] = [friend]
            }
        }
        
        friendsSectionTitles = [String] (friendsDictionary.keys)
        friendsSectionTitles = friendsSectionTitles.sorted(by: { $0 < $1 })
            
        tableView.register(FriendCell.self, forCellReuseIdentifier: FriendCell.reuseIdentifier)
        tableView.rowHeight = 80
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return isFiltering ? 1 : friendsSectionTitles.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filteredFriends.count
        }
        
        guard let friendsValues = friendsDictionary[friendsSectionTitles[section]] else {
            return 0
        }
        
        return friendsValues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FriendCell.reuseIdentifier, for: indexPath) as! FriendCell
        
        let dataSource = isFiltering ? filteredFriends : friendsDictionary[friendsSectionTitles[indexPath.section]]!
        cell.setAvatar(dataSource[indexPath.row].avatar)
        cell.setName(dataSource[indexPath.row].name)
        cell.setId(dataSource[indexPath.row].id)
        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return isFiltering ? filteredSectionTitle : friendsSectionTitles[section]
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return  isFiltering ? [filteredSectionTitle] : friendsSectionTitles
    }
    
    
    // MARK: - Table View Delegate


    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard !isFiltering else {
            return UISwipeActionsConfiguration()
        }
        return UISwipeActionsConfiguration(actions: [makeDeleteContextualAction(indexPath)])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let friendPhotos = UserPhotosCollectionVC(collectionViewLayout: UICollectionViewFlowLayout())
        let currentSection = isFiltering ? friendsDictionary[filteredSectionTitle]! : friendsDictionary[friendsSectionTitles[indexPath.section]]!
        friendPhotos.setCurrentUser(currentSection[indexPath.row])
        
        navigationController?.pushViewController(friendPhotos, animated: true)
    }
    
    // MARK: - Helpers Functions
    
    func createFriendsArray() {
        friends.append(UserModel(avatar: UIImage(named: "ricardo_4")!, name: "Ricardo", id: 1))
        friends.append(UserModel(avatar: UIImage(named: "shrek_3")!, name: "Shrek", id: 2))
        friends.append(UserModel(avatar: UIImage(named: "michael_1")!, name: "Michel", id: 3))
        friends.append(UserModel(avatar:  UIImage(named: "zhak_1")!, name: "Zhak", id: 4))
        friends.append(UserModel(avatar:  UIImage(named: "frog_1")!, name: "Frog", id: 5))
        friends.append(UserModel(avatar:  UIImage(named: "peter_1")!, name: "Peter", id: 6))
        friends.append(UserModel(avatar:  UIImage(named: "shon_1")!, name: "Shon", id: 7))
        friends.append(UserModel(avatar:  UIImage(named: "gigi_1")!, name: "Gigi", id: 8))
    }
    
    private func makeDeleteContextualAction(_ indexPath: IndexPath) -> UIContextualAction {

        return UIContextualAction(style: .destructive, title: "Delete") { (action, swipeButtonView, completion) in
            
            self.tableView.beginUpdates()
            print(indexPath)
            let currentSectionKey = self.friendsSectionTitles[indexPath.section]
            var currentSection = self.friendsDictionary[currentSectionKey]!
            currentSection.remove(at: indexPath.row)
            
            if currentSection.count == 0 {
                self.friendsSectionTitles.remove(at: indexPath.section)
                self.friendsDictionary.removeValue(forKey: currentSectionKey)
                let indexSet = IndexSet(arrayLiteral: indexPath.section)
                self.tableView.deleteSections(indexSet, with: .fade)
            } else {
                self.friendsDictionary.updateValue(currentSection, forKey: currentSectionKey)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
            self.tableView.endUpdates()
            
            print(indexPath)
            
            completion(true)
        }
    }
    
}

extension MyFriendsTableVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard searchController.isActive else {
            return
        }
        
        guard let text = searchController.searchBar.text else { return }
        filterContentForSearchText(text)
    }
    
    func filterContentForSearchText(_ searchText: String) {
       
        let firstSearchLetter = String(searchText.prefix(1)).uppercased()
        
        if friendsSectionTitles.contains(firstSearchLetter) {
            filteredSectionTitle = firstSearchLetter
        }
        
        filteredFriends = friendsDictionary[filteredSectionTitle]?.filter { $0.name.lowercased().contains(searchText.lowercased()) } ?? []
        tableView.reloadData()
    }
}

