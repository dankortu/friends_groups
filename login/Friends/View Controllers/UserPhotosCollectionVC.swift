//
//  FriendPhotosCollectionVCCollectionViewController.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class UserPhotosCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Properties
    
    private var currentUser: UserModel?
    private var imagesArr: [UIImage] = [UIImage]()
//    weak var delegate: MyFriendsVC?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: PhotoCell.reuseIdentifier)
        collectionView.backgroundColor = .white
        imagesArr = makeImages(currentUser?.id)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    // MARK: Collection View Data Source
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imagesArr.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.reuseIdentifier, for: indexPath) as! PhotoCell
        cell.setUserPhoto(imagesArr[indexPath.row])
        
        return cell
    }
    
    // MARK: Flow Layout Delegate
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedPhoto = imagesArr[indexPath.row]
        let userPhoto = UserPhotoVC()
        userPhoto.setSelectedPhoto(selectedPhoto)
        
        navigationController?.pushViewController(userPhoto, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSide = collectionView.frame.width / 2
        
        return CGSize(width: cellSide, height: cellSide)
    }
    
    // row spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    // column spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: - Utility Functions
    
    func setCurrentUser(_ user: UserModel) {
        self.currentUser = user
    }
    
    private func makeImages(_ id: Int?) -> [UIImage] {
        
        var images = [UIImage]()
        
        if let id = id {
            switch id {
            case 1:
                images.append(UIImage(named: "ricardo_1")!)
                images.append(UIImage(named: "ricardo_2")!)
                images.append(UIImage(named: "ricardo_3")!)
                images.append(UIImage(named: "ricardo_4")!)
                images.append(UIImage(named: "ricardo_5")!)
                
            case 2:
                images.append(UIImage(named: "shrek_1")!)
                images.append(UIImage(named: "shrek_2")!)
                images.append(UIImage(named: "shrek_3")!)
                images.append(UIImage(named: "shrek_4")!)
                images.append(UIImage(named: "shrek_5")!)
            default:
                print("Нет такого юзера")
            }
        }
        
        return images
    }
    
}

//protocol UserPhotoCollectionVCDelegate: AnyObject {
//    func setNewAvatar(_ newAvatar: UIImage, _ userId: Int?)
//}
