//
//  LikeControl.swift
//  login
//
//  Created by Daniil Kortunov on 01.04.2021.
//

import UIKit

class LikeControl: UIControl {
    
    private var likeButton = UIButton()
    private var likesCountLab = UILabel()
    
    
    private var isLiked = false
    private var likesCount = 0 {
        didSet {
            likesCountLab.text = String(likesCount)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    private func setupView() {
        addSubview(likeButton)
        addSubview(likesCountLab)
        setupPropertiesLikesCountLab()
        setupPropertiesLikeButton()
        print(self.frame.size.width)
    }
    
}

extension LikeControl {
    private func setupPropertiesLikesCountLab() {
        
        likesCountLab.text = String(likesCount)
        likesCountLab.textColor = .white
        likesCountLab.font = UIFont(name: "Helvetica-Bold", size: 30)
        
        likesCountLab.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            likesCountLab.leftAnchor.constraint(equalTo: likeButton.rightAnchor, constant: 5),
            likesCountLab.bottomAnchor.constraint(equalTo: bottomAnchor),
            likesCountLab.widthAnchor.constraint(equalToConstant: 30),
            
        ])
    }
    
    private func setupPropertiesLikeButton() {
        
        likeButton.setImage(UIImage(systemName: "suit.heart.fill"), for: .normal)
        likeButton.imageView?.contentMode = .scaleAspectFit
        likeButton.contentMode = .scaleAspectFit
        likeButton.contentHorizontalAlignment = .fill
        likeButton.contentVerticalAlignment = .fill
        likeButton.addTarget(self, action: #selector(handleLikeControlTapped), for: .touchUpInside)
        
        
        setTintColorLikeButton()
        
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        
        let iconSide: CGFloat = 35
        
        NSLayoutConstraint.activate([
            likeButton.leftAnchor.constraint(equalTo: leftAnchor),
            likeButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            likeButton.heightAnchor.constraint(equalToConstant: iconSide),
            likeButton.widthAnchor.constraint(equalTo: likeButton.heightAnchor, constant: 10)
        ])

    }
    
    @objc
    func handleLikeControlTapped() {
        isLiked = !isLiked
        setTintColorLikeButton()
        
        if isLiked {
            likesCount += 1
        } else if likesCount > 0 {
            likesCount -= 1
        }
    }
    
    private func setTintColorLikeButton() {
        likeButton.tintColor =  isLiked ? .red : .gray
    }
    
}
