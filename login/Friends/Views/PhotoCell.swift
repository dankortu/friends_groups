//
//  PhotoCollectionViewCell.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class PhotoCell: UICollectionViewCell, Reuseble {
    
    private var photoIV = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        contentView.addSubview(photoIV)
        
        photoIV.translatesAutoresizingMaskIntoConstraints = false
        photoIV.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
        photoIV.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        
        photoIV.contentMode = .scaleAspectFill
        contentView.clipsToBounds = true


    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUserPhoto(_ image: UIImage) {
        photoIV.image = image
    }
}


