//
//  FriendView.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class FriendCell: UITableViewCell, Reuseble {
    
    private var friendAvatarIV = AvatarView()
    private var friendNameLab = UILabel()
    private var friendIdLab = UILabel()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(friendAvatarIV)
        contentView.addSubview(friendNameLab)
        contentView.addSubview(friendIdLab)
                
        friendAvatarIV.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, width: 70)
        friendNameLab.anchor(top: topAnchor, left: friendAvatarIV.rightAnchor, paddingTop: 20, paddingLeft: 10, width: frame.size.width / 2)
        friendIdLab.anchor(top: friendNameLab.bottomAnchor, left: friendAvatarIV.rightAnchor, paddingLeft: 10, width: frame.size.width / 2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAvatar(_ image: UIImage) {
        friendAvatarIV.setImage(image)
    }
    
    func setName(_ name: String) {
        
        var myMutableString = NSMutableAttributedString(string: name)
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:0,length:4))
        friendNameLab.attributedText = myMutableString
        
        let customFont = UIFont(name: "Montserrat-Light", size: UIFont.labelFontSize)
        friendNameLab.font = customFont
    }
    
    func setId(_ id: Int) {
        friendIdLab.text = String(id)
    }

}

