//
//  FriendModel.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class UserModel {
    
    var avatar: UIImage
    let name: String
    let id: Int

    
    init(avatar: UIImage, name: String, id: Int) {
        
        self.avatar = avatar
        self.name = name
        self.id = id
    }
    
}
