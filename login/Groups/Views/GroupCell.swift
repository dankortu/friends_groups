//
//  GroupCellTableViewCell.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

final class GroupCell: UITableViewCell, Reuseble {
    
    //MARK: - Properties
    
    weak var delegate: GroupCellDelegate?
    
    private var avatarView = AvatarView()
    private var nameLab = UILabel()
    private var idLab = UILabel()
    private var subscibersNumberLab = UILabel()
    private var subscribeBtn = UIButton()
    
    private var isSubscribed: Bool = false {
        
        didSet {
            if isSubscribed {
                subscribeBtn.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            } else {
                subscribeBtn.setImage(UIImage(systemName: "plus.app"), for: .normal)
            }
        }
    }
    
    //MARK: - Subview & Constaints
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(avatarView)
        contentView.addSubview(nameLab)
        contentView.addSubview(idLab)
        contentView.addSubview(subscibersNumberLab)
        contentView.addSubview(subscribeBtn)

        avatarView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor,
                        paddingTop: 5, paddingLeft: 5, paddingBottom: 5,
                        width: 70)

        nameLab.anchor(top: topAnchor, left: avatarView.rightAnchor,
                       paddingTop: 10, paddingLeft: 10,
                       width: frame.size.width * 0.6)

        idLab.anchor(top: nameLab.bottomAnchor, left: avatarView.rightAnchor,
                     paddingLeft: 10,
                     width: frame.size.width * 0.6)
        
        idLab.adjustsFontForContentSizeCategory = true

        subscibersNumberLab.anchor(top: idLab.bottomAnchor, left: avatarView.rightAnchor,
                                   paddingLeft: 10,
                                   width: frame.size.width * 0.6)
        
        subscribeBtn.translatesAutoresizingMaskIntoConstraints = false
        subscribeBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        subscribeBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        subscribeBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        subscribeBtn.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
    
        subscribeBtn.setImage(UIImage(systemName: "plus.app"), for: .normal)
        subscribeBtn.imageView?.contentMode = .scaleAspectFit
        subscribeBtn.contentHorizontalAlignment = .fill
        subscribeBtn.contentVerticalAlignment = .fill
        subscribeBtn.addTarget(self, action: #selector(handleSubscribeButtonTap), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        print("cell is preparing for reuse")
    }
    
    //MARK: - Set values in SubView
    
    func setAvatar(_ avatar: UIImage) {
        avatarView.setImage(avatar)
    }
    
    func setName(_ name: String) {
        self.nameLab.text = name
    }
    
    func setId(_ id: Int) {
        self.idLab.text = "ID: " + String(id)
    }
    
    func setSubscribersNumber(_ subscribersNumber: Int) {
        self.subscibersNumberLab.text = "Подписано: " + String(subscribersNumber) + " чел."
    }
    
    func updateSubscription(isSubscribed: Bool) {
        
        self.isSubscribed = isSubscribed
    }
    
    //MARK: - Helpers functions
    
    @objc
    private func handleSubscribeButtonTap() {
        
        isSubscribed.toggle()
        delegate?.didUpdateSubscription(self, isSubscribed: isSubscribed)
    }
}

protocol GroupCellDelegate: AnyObject {
            
    func didUpdateSubscription(_ cell: GroupCell, isSubscribed: Bool)
}

