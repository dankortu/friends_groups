//
//  GroupModel.swift
//  login
//
//  Created by Daniil Kortunov on 30.03.2021.
//

import UIKit

class GroupModel {
    
    let avatar: UIImage
    let name: String
    let subscribersNumber: Int
    let id: Int
    var isSubscribed: Bool
    
    init(avatar: UIImage, name: String, subscribersNumber: Int, id: Int, isSubscribed: Bool = false) {
        
        self.avatar = avatar
        self.name = name
        self.subscribersNumber = subscribersNumber
        self.id = id
        self.isSubscribed = isSubscribed
    }
    
}

extension GroupModel: Equatable {
    
    static func == (lhs: GroupModel, rhs: GroupModel) -> Bool {
        return lhs.id == rhs.id
    }
}
