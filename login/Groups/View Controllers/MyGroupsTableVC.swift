//
//  GroupsViewController.swift
//  login
//
//  Created by Daniil Kortunov on 29.03.2021.
//

import UIKit

final class MyGroupsTableVC: UITableViewController {
    
    // MARK: - Properties
    
    private var groups = [GroupModel]()
    
    private let allGroupsVC = AllGroupsVC()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Мои группы"
        groups = GroupsFactory.makeGroupsArr(mode: .myGroups)
        allGroupsVC.delegate = self
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(barButtonTapped))
        barButton.title = "Add"
        navigationItem.rightBarButtonItem = barButton
        
        tableView.register(GroupCell.self, forCellReuseIdentifier: GroupCell.reuseIdentifier)
        tableView.rowHeight = 80
        tableView.tableFooterView = UIView()
        
    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        return groups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GroupCell.reuseIdentifier, for: indexPath) as! GroupCell
        
        cell.delegate = self
        
        cell.setAvatar(groups[indexPath.row].avatar)
        cell.setName(groups[indexPath.row].name)
        cell.setId(groups[indexPath.row].id)
        cell.setSubscribersNumber(groups[indexPath.row].subscribersNumber)
        cell.updateSubscription(isSubscribed: groups[indexPath.row].isSubscribed)
        
        return cell
    }
    
    // MARK: - Helpers functions
    
    @objc
    private func barButtonTapped() {
        
        navigationController?.pushViewController(allGroupsVC, animated: true)
    }
}

extension MyGroupsTableVC: GroupCellDelegate {
    
    func didUpdateSubscription(_ cell: GroupCell, isSubscribed: Bool) {
        
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        guard indexPath.row <= groups.count else {
            return
        }
        
        let requiredGroup = groups[indexPath.row]
        requiredGroup.isSubscribed = isSubscribed
        
        groups.removeAll(where: { $0 == requiredGroup })
        allGroupsVC.updateGroupSubscription(requiredGroup)
        
        tableView.reloadData()
    }
    
}

extension MyGroupsTableVC: AllGroupsVCDelegate {
    
    func didSubscribedToGroup(_ group: GroupModel) {
        
        guard groups.contains(where: { $0 == group }) == false else {
            return
        }
        
        groups.append(group)
        tableView.reloadData()
    }
    
    func didUnsubscribedFromGroup(_ group: GroupModel) {
        
        guard groups.contains(where: { $0 == group }) else {
            return
        }
        
        guard let index = groups.firstIndex(where: { $0 == group }) else {
            return
        }
        
        groups.remove(at: index)
        tableView.reloadData()
    }
}
