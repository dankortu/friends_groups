//
//  AllGroupsVC.swift
//  login
//
//  Created by Daniil Kortunov on 31.03.2021.
//

import UIKit

final class AllGroupsVC: UITableViewController {
    
    // MARK: - Properties
    
    private var groups = [GroupModel]()
    
    weak var delegate: AllGroupsVCDelegate?
    
    // MARK: - Initialization
    
    init() {
    
        super.init(nibName: nil, bundle: nil)
        
        groups = GroupsFactory.makeGroupsArr(mode: .allGroups)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(GroupCell.self, forCellReuseIdentifier: GroupCell.reuseIdentifier)
        tableView.rowHeight = 80
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return groups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GroupCell.reuseIdentifier, for: indexPath) as! GroupCell
        
        cell.delegate = self
        
        cell.setAvatar(groups[indexPath.row].avatar)
        cell.setName(groups[indexPath.row].name)
        cell.setId(groups[indexPath.row].id)
        cell.setSubscribersNumber(groups[indexPath.row].subscribersNumber)
        cell.updateSubscription(isSubscribed: groups[indexPath.row].isSubscribed)
        
        return cell
    }
    
    func updateGroupSubscription(_ group: GroupModel) {
        
        if let groupToUpdate = groups.first(where: { $0 == group }) {
            groupToUpdate.isSubscribed = group.isSubscribed
            
            tableView.reloadData()
        }
    }
}

extension AllGroupsVC: GroupCellDelegate {
    
    func didUpdateSubscription(_ cell: GroupCell, isSubscribed: Bool) {
        
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        guard indexPath.row <= groups.count else {
            return
        }
        
        let requiredGroup = groups[indexPath.row]
        requiredGroup.isSubscribed = isSubscribed
        
        if isSubscribed {
            delegate?.didSubscribedToGroup(requiredGroup)
        } else {
            delegate?.didUnsubscribedFromGroup(requiredGroup)
        }
    }
}

protocol AllGroupsVCDelegate: AnyObject {
    
    func didSubscribedToGroup(_ group: GroupModel)
    
    func didUnsubscribedFromGroup(_ group: GroupModel)
}
