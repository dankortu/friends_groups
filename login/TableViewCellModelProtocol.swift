//
//  TableViewCellProtocol.swift
//  login
//
//  Created by Daniil Kortunov on 02.04.2021.
//

import UIKit

protocol TableViewCellModelProtocol {
    func setAvatar(_ avatar: UIImage)
    func setName(_ name: String)
    func setId(_ id: Int)
}
