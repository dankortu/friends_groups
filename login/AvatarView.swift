//
//  Avatar.swift
//  login
//
//  Created by Daniil Kortunov on 01.04.2021.
//

import UIKit

final class AvatarView: UIView {
    
    private var imageView = UIImageView()
    private var shadowView = UIView()
    
    init() {
        super.init(frame: .zero)
        
        addSubview(shadowView)
        addSubview(imageView)

        imageView.layer.cornerRadius = frame.size.width / 2
        imageView.clipsToBounds = true
        imageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        
        shadowView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = .zero
        shadowView.backgroundColor = .white
        shadowView.layer.masksToBounds = false
        shadowView.layer.cornerRadius = imageView.layer.cornerRadius
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    func setImage(_ image: UIImage) {
        imageView.image = image
    }
    
    private func updateCornerRadius() {
        
        let selfWidth = self.bounds.size.width
        let selfHeight = self.bounds.size.height
        
        imageView.layer.cornerRadius = selfWidth > selfHeight ? selfHeight / 2 : selfWidth / 2
        shadowView.layer.cornerRadius = selfWidth > selfHeight ? selfHeight / 2 : selfWidth / 2
    }
}
