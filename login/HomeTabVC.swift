//
//  TabBarController.swift
//  login
//
//  Created by Daniil Kortunov on 29.03.2021.
//

import UIKit

final class HomeTabVC: UITabBarController {
    let friendsViewController = MyFriendsTableVC()
    let groupsViewController = MyGroupsTableVC()
    let userPhotoVC = UserPhotoVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = "Tab bar vc"
        
        friendsViewController.tabBarItem = UITabBarItem(title: TypeSelectedTab.friends.rawValue, image: UIImage(systemName: "person.3.fill")!, tag: 0)
        groupsViewController.tabBarItem = UITabBarItem(title: TypeSelectedTab.groups.rawValue, image: UIImage(systemName: "person.2.circle")!, tag: 1)
    
        let friendsNavVC = UINavigationController(rootViewController: friendsViewController)
        let groupsNavVC = UINavigationController(rootViewController: groupsViewController)
        
        let viewControllersList = [friendsNavVC, groupsNavVC]
        viewControllers = viewControllersList
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    

}

extension HomeTabVC {
    enum TypeSelectedTab: String {
        case friends = "Друзья"
        case groups = "Группы"
    }
}
